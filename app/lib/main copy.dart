import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  
  static const List<Point> pointList = [ Point(1.0, 1.0), Point(2.0, 2.0), Point(3.0, 1.5)];
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage('Greenie Genie', pointList),
    );
  }
}
class Point {
  final double x;
  final double y;
  const Point(this.x, this.y);
}

class MyHomePage extends StatelessWidget {
  const MyHomePage(this.title, this.points, {Key? key}) :super(key: key);

  final List<Point> points;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 2,
              child :
                LineChart(
                  LineChartData (
                    lineBarsData: [
                      LineChartBarData (
                        spots: points.map((point) => FlSpot(point.x, point.y)).toList(),
                        isCurved: true,
                      ),
                    ],
                  ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.area_chart_sharp), label:""),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label:""),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label:""),
        ],
      ),
    );
  }
}
