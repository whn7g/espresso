import 'dart:convert';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

const Map<int, Color> browns = {
  50:Color.fromRGBO(136,14,79, .1),
  100:Color.fromRGBO(136,14,79, .2),
  200:Color.fromRGBO(136,14,79, .3),
  300:Color.fromRGBO(136,14,79, .4),
  400:Color.fromRGBO(136,14,79, .5),
  500:Color.fromRGBO(136,14,79, .6),
  600:Color.fromRGBO(136,14,79, .7),
  700:Color.fromRGBO(136,14,79, .8),
  800:Color.fromRGBO(136,14,79, .9),
  900:Color.fromRGBO(136,14,79, 1),
};
MaterialColor colorCustom = MaterialColor(0xFF2A6133, browns);
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  
  static const List<Point> pointList = [ Point(1.0, 1.0), Point(2.0, 2.0), Point(3.0, 1.5)];
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: colorCustom,
        scaffoldBackgroundColor: Color.fromARGB(255, 90, 74, 58),
        textTheme: TextTheme(
          headline2: TextStyle(color: Colors.white),
          bodyText1: TextStyle(color: Colors.white),
          bodyText2: TextStyle(color: Colors.white),
        ),
      ),
      home: MyHomePage(title: 'Espresso'),
      routes: {
        AdviceWidget.route: (context) { return Scaffold(body: AdviceWidget());},
      },
    );
  }
}
class Point {
  final double x;
  final double y;
  const Point(this.x, this.y);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title});
  String title;

  @override
  State<MyHomePage> createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      if(_selectedIndex == 0 && index == 0) { Navigator.pushNamed(context,'/');}
      _selectedIndex = index;
    });
  }

  Future<Widget> getRequest(String sensor) async {
    String url = "http://192.168.1.1/"+sensor+"_hist";
    final response = await http.get(Uri.parse(url));
    var responseData = json.decode(response.body);
    List<int> myTimeStamp = [];
    List<double> myData = [];
    try {
      myTimeStamp = responseData["timestamps"].cast<int>();
      myData = responseData["data"].cast<double>();
    } catch (e){
      print(e);
    }
    String myTitle = "";
    switch (sensor) {
      case "temp":
        myTitle = "Temperature (degrees   C)";
      break;
      case "humidity":
        myTitle = "Humidity (%)";
      break;
      case "pressure":
        myTitle = "Pressure (PSI)";
      break;
      case "co2":
        myTitle = "CO2 (PPM)";
      break;
      case "voc":
        myTitle = "VOC (PPB)";
      break;
      case "light":
        myTitle = "Light (0-255)";
      break;
      default:
    }
    History history = History(timestamps: myTimeStamp, data: myData, title: myTitle);
    return history;
  }

  FutureBuilder futureData(String sensor) {
    return FutureBuilder (
      future: getRequest(sensor),
      builder: (BuildContext ctx, AsyncSnapshot snapshot) {
        if(snapshot.data == null) {
          return Container(
            child: Center(
              child: Text("Loading"),
            ),
          );
        } else {
          History history = snapshot.data;
          print("Hitting the endpoint :)");
          return snapshot.data;
        }
      }
    );
  }
  @override
  Widget build(BuildContext context) {

    final FutureBuilder temp = futureData("temp");
    final FutureBuilder humidity = futureData("humidity");
    final FutureBuilder pressure = futureData("pressure");
    final FutureBuilder co = futureData("co");
    final FutureBuilder voc = futureData("voc");
    final FutureBuilder light = futureData("light");

    double dist = 70;
    final Column col = Column(
      children: [
        SizedBox(height: dist,),
        temp,
        SizedBox(height: dist,),
        humidity,
        SizedBox(height: dist,),
        pressure,
        SizedBox(height: dist,),
        co,
        SizedBox(height: dist,),
        voc,
        SizedBox(height: dist,),
        light,
        SizedBox(height: dist,),
      ],
    );
    List<Widget> _widgetOptions = <Widget>[col, AdviceWidget(), SettingsWidget()];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: 
       Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _widgetOptions.elementAt(_selectedIndex),
          ],
        ),
      ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.area_chart_sharp), label:"Data"),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label:"Advice"),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label:"Settings"),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}

class Graph extends StatelessWidget {
  
  const Graph(this.points, this.title, {Key? key}) :super(key: key);

  final List<Point> points;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
            children: [
              Text(title),
              Row(children: [
                Expanded(flex: 1, child: Container(),),
                Expanded(flex: 8, child: 
                  AspectRatio(
                    aspectRatio: 3,
                    child :
                      LineChart(
                        LineChartData (
                          titlesData: titlesData2,
                          lineBarsData: [
                            LineChartBarData (
                              dotData: FlDotData(show: false),
                              spots: points.map((point) => FlSpot(point.x, point.y)).toList(),
                              isCurved: false,
                              
                            ),
                          ],
                        ),
                    ),
                  ),
                ),
              Expanded(flex: 1, child: Container(),),
              ])
            ]
          );
  }
}

double myAverage(List<double> values) {
  double sum = 0;
  for(double value in values) {
    sum = sum + value;
  }
  return sum/values.length;
}
class AdviceWidget extends StatelessWidget {
  static const route = "/advice";
  const AdviceWidget({Key? key}) : super(key: key);
  static History test = History(timestamps: [1681590123], data: []);

  Future<Widget> getRequest(String sensor) async {
    String url = "http://192.168.1.1/"+sensor+"_hist";
    final response = await http.get(Uri.parse(url));
    var responseData = json.decode(response.body);
    List<int> myTimeStamp = [];
    List<double> myData = [];
    try {
      myTimeStamp = responseData["timestamps"].cast<int>();
      myData = responseData["data"].cast<double>();
    } catch (e){
      print(e);
    }
    String myAdvice = "";
    switch (sensor) {
      case "temp":
        if(myAverage(myData) < 10){
          myAdvice = "Turn up the heat";
        }
      break;
      case "humidity":
        if(myAverage(myData) > 30){
          myAdvice = "Get a dehumidifier";
        }
      break;
      case "pressure":
        if(myAverage(myData) < 10){
          myAdvice = "Turn up the heat";
        }
      break;
      case "co2":
        if(myAverage(myData) < 10){
          myAdvice = "Turn up the heat";
        }
      break;
      case "voc":
        if(myAverage(myData) < 10){
          myAdvice = "Turn up the heat";
        }
      break;
      case "light":
        if(myAverage(myData) < 10){
          myAdvice = "Turn up the heat";
        }
      break;
      default:
    }
    Advice history = Advice(timestamps: myTimeStamp, data: myData, advice: myAdvice);
    return history;
  }

  FutureBuilder futureData(String sensor) {
    return FutureBuilder (
      future: getRequest(sensor),
      builder: (BuildContext ctx, AsyncSnapshot snapshot) {
        if(snapshot.data == null) {
          return Text("");
        } else {
          Advice advice = snapshot.data;
          print("Hitting the endpoint :)");
          return snapshot.data;
      }
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    //List<Widget> advice = [];

    final FutureBuilder temp = futureData("temp");
    final FutureBuilder humidity = futureData("humidity");
    final FutureBuilder pressure = futureData("pressure");
    final FutureBuilder co = futureData("co");
    final FutureBuilder voc = futureData("voc");
    final FutureBuilder light = futureData("light");

    return Column(children: [temp, humidity, pressure, co, voc, light]);
      // SizedBox(height: 10,),
      // Text("Touch grass"),
      // SizedBox(height: 10,),
      // Text("Open windows"),
      // SizedBox(height: 10,),
      // Text("Open your curtains to get more light"),
      // SizedBox(height: 10,),
      // Text("Go outside"),
      // SizedBox(height: 10,),
      // Text("Use a dehumidifier. Humid air causes mold."),);
  }
}

class SettingsWidget extends StatelessWidget {
  static const route = "/settings";
  const SettingsWidget({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text("About"),
      Text("Front End Created by: Austen Solvie"),
      Text("Backend API Created by: Liam Helfrich & Tyler Lunyou"),
      Text("Safety System Created by: Kshitij Sharma"),
      Text("Housing Designed by: "),
      SizedBox(height: 10,),
      Text("Contact"),
      Text("Austen : adsymk@umsystem.edu"),
      Text("Kshitij : ksssgy4@umsystem.edu"),
      Text("Liam : whn7g@umsystem.edu"),
      Text("Tyler : tjlkdr@umsystem.edu")
    ]);
  }
}

class History extends StatelessWidget{
  final List<int> timestamps;
  final List<double> data;
  final String title;
  History({
    this.timestamps = const [],
    this.data = const [],
    this.title = "",
  });
  double Average() {
    double sum = 0;
    for(double value in data){
      sum = sum + value;
    }
    return sum/data.length;
  }
  @override
  Widget build(BuildContext  context) {
    //List<Widget> times = [];
    List<Point> timesData = [];
    for(int i = 0; i < timestamps.length; i++) {
      if(this.data.elementAt(i) != 0) {
        timesData.add(Point(timestamps.elementAt(i).toDouble(), data.elementAt(i)));
      }
      //times.add(Text(readTimestamp(timestamps.elementAt(i)) + " : " + this.data.elementAt(i).toString()));
    }
    return Graph(timesData, title);
 //   return Column(children: times);
  }
}

class Advice extends StatelessWidget{
  final List<int> timestamps;
  final List<double> data;
  final String advice;
  Advice({
    this.timestamps = const [],
    this.data = const [],
    this.advice = "",
  });

  double Average() {
    double sum = 0;
    for(double value in data){
      sum = sum + value;
    }
    return sum/data.length;
  }
  
  @override
  Widget build(BuildContext  context) {
    return Column(children: [SizedBox(height:10), Text(advice)]);
  }
}

List<String> readTimestamps(List<int> timestamps) {
  List<String> dates = [];
  for (var element in timestamps) {
    dates.add(DateTime.fromMillisecondsSinceEpoch(element * 1000).toString());
  }
  return dates;
}

String readTimestamp(int timestamp) {
  final DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  return date.toString();
}

FlTitlesData get titlesData2 => FlTitlesData(
  bottomTitles: AxisTitles(
    sideTitles: bottomTitles
  ),
  rightTitles: AxisTitles(
    sideTitles: SideTitles(showTitles: false),
  ),
  topTitles: AxisTitles(
    sideTitles: SideTitles(showTitles: false),
  ),
);

Widget bottomTitlesWidgets (double value, TitleMeta meta) {
  const style = TextStyle(
    fontSize: 10,
  );
  Widget text = Text(readTimestamp(value.toInt()), style:style);
  return SideTitleWidget(
    axisSide: meta.axisSide,
    // space: 10,
    child: text,
    angle: 3.1415*3/2+0.3,
  );
}

SideTitles BottomTitles(int ) {
  return SideTitles(
    showTitles: true,
    interval: 12,
    getTitlesWidget: bottomTitlesWidgets,
  );
}
SideTitles get bottomTitles => SideTitles(
  showTitles: true,
  // interval: 12,
  getTitlesWidget: bottomTitlesWidgets,
);