#include "driver/ledc.h"
#include "esp_err.h"

#define LEDC_TIMER              LEDC_TIMER_0
#define LEDC_MODE               LEDC_LOW_SPEED_MODE
#define LEDC_OUTPUT_IO          (32) // Define the output GPIO
#define LEDC_CHANNEL            LEDC_CHANNEL_0
#define LEDC_DUTY_RES           LEDC_TIMER_13_BIT // Set duty resolution to 13 bits
#define LEDC_DUTY               (4095) // Set duty to 50%. ((2 ** 13) - 1) * 50% = 4095
#define LEDC_FREQUENCY          (500) // Frequency in Hertz. Set frequency at 5 kHz

//Timer pointer
hw_timer_t* timer = NULL;

void IRAM_ATTR TimerInterrupt(); //Prototype for the interrupt source

void setup() {
  Serial.begin(115200);  

  /**** Set up the ESP32's LED controller to generate a test audio signal *****/
  
  // Prepare and then apply the LEDC PWM timer configuration
  ledc_timer_config_t ledc_timer = {
    /*speed_mode:*/      LEDC_MODE, 
    /*duty_resolution:*/ LEDC_DUTY_RES,
    /*timer_num:*/       LEDC_TIMER, 
    /*freq_hz:*/         LEDC_FREQUENCY
  };
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  ledc_timer.timer_num = LEDC_TIMER_1;
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  // Prepare and then apply the LEDC PWM channel configuration
  ledc_channel_config_t ledc_channel = {
    LEDC_OUTPUT_IO,
    LEDC_MODE,
    LEDC_CHANNEL_0,
    LEDC_INTR_DISABLE,
    LEDC_TIMER,
    0, // Set duty to 0%
    0
  };

  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));

  ledc_channel.channel = LEDC_CHANNEL_1;
  ledc_channel.gpio_num = 25;
  
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));



  /**** Set up the timer interrupt ****/
  timer = timerBegin(0, 80, true); //Select timer 0, set the prescaler to 80, and enable counting up to TRUE
  
  timerAttachInterrupt(timer, &TimerInterrupt, true); //Passing a pointer to timer, the address of the ISR, and then enable the trigger for the interrupt
  
  timerAlarmEnable(timer); //Enable the timer interrupt
  
  timerAlarmWrite(timer, 1000000, true); //Raise the interrupt at 1 second (second parameter)
}

void IRAM_ATTR TimerInterrupt() {
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, LEDC_DUTY);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  delay(1000);
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, LEDC_DUTY/2);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  delay(1000);

  return;	
}

void loop()
{


}