#include <WiFi.h>
#include <WebServer.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <bsec.h>
#include "RTClib.h"
#include "SparkFunCCS811.h" //Click here to get the library: http://librarymanager/All#SparkFun_CCS811
#include "driver/ledc.h"
#include "esp_err.h"

/*
Buzzer timer LED controller constants
*/
#define LEDC_TIMER              LEDC_TIMER_0        //Which hardware timer to use
#define LEDC_MODE               LEDC_LOW_SPEED_MODE //Timer speed mode
#define LEDC_CHANNEL            LEDC_CHANNEL_0      //Which channel to use
#define LEDC_DUTY_RES           LEDC_TIMER_13_BIT   // Set duty resolution to 13 bits
#define LEDC_DUTY               (4095)              // Set duty to 50%. ((2 ** 13) - 1) * 50% = 4095
#define LEDC_FREQUENCY          (1000)              // Frequency in Hertz. Set frequency at 5 kHz

/*
Pin constants
*/
#define LEDC_OUTPUT_IO    27 // Buzzer pin
#define INDICATOR_LED_PIN 14 // Warning indicator LED pin
#define LIGHTPIN          34 // Photoresistor pin

/*
Other constants
*/
#define CCS811_ADDR       0x5B //Default I2C Address for CCS811

/*
Sensor objects
*/
CCS811      ccs811_sensor(CCS811_ADDR);
Bsec        bme680_sensor; //BME680
RTC_PCF8523 rtc;

/* WiFi constants */
const char* ssid = "ESPRESSO";      // WiFi network SSID
const char* password = "12345678";  // WiFi network password

IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

WebServer server(80);

/*
Objects for processing sensor data
*/
StaticJsonDocument<4096> doc; //Object for translating data into json
String result_string; //String object for holding the results of json serialization

struct SensorStateReading{
  /*
  Represents all of the sensor readings at a particular timestamp
  */
  uint32_t timestamp;
  uint8_t light;
  float temp;
  float humidity;
  float pressure;
  uint16_t co2;
  uint16_t voc;
  uint8_t quality; 

  SensorStateReading(){
    /*
    Constructor to initialize all values to zero
    */
    timestamp = 0;
    light = 0;
    temp = 0;
    humidity = 0;
    pressure = 0;
    co2 = 0;
    voc = 0;
    quality = 0;
  }

  String serialize_json(){
    /*
    Return a json string containing all of the sensor readings
    */
    doc.clear();
      
    doc["timestamp"] = timestamp;
    doc["light"] = light;
    doc["temp"] = temp;
    doc["humidity"] = humidity;
    doc["pressure"] = pressure;
    doc["cotwo"] = co2;
    doc["voc"] = voc;
    doc["quality"] = quality;

    String result = "";
    serializeJson(doc, result);

    return result;
  }
};

/*
Sensor data storage
*/
#define MAX_HIST_SZ 144                   //Maximum number of snapshots that will be stored in memory
SensorStateReading history[MAX_HIST_SZ];  //Historical sensor readings array
int      num_readings = 0;                //Number of snapshots in memory 
uint32_t read_interval = 10000;           //Read every 10 seconds
uint32_t last_read = 0;                   //millis() time at which last reading was taken

SensorStateReading* getLastReading(){
  /*
  Return the last valid reading in the array.
  */
  if(num_readings > 0){
    return history + (num_readings - 1);
  }
}

void getCurrentState(SensorStateReading* out_state){
  /*
  Read values from all of the sensors and populate out_state with them  
  */
  read_BME680(out_state);
  read_CCS811(out_state);
    
  //get time and set 
  DateTime now = rtc.now();
  out_state->timestamp = now.unixtime();

  int lightval = 0;
  lightval = analogRead(LIGHTPIN);
  out_state->light = static_cast<uint8_t>(map(lightval,0,4096,0,255));
}

void check_sensor_status(){
  /*
  Check if there is a sensor error; if so, print out error code.
  */
  String out_str;
  if (bme680_sensor.status != BSEC_OK) {
    if (bme680_sensor.status < BSEC_OK) {
      out_str = "BSEC error code : " + String(bme680_sensor.status);
      Serial.println(out_str);

    } else {
      out_str = "BSEC warning code : " + String(bme680_sensor.status);
      Serial.println(out_str);
    }
  }

  if (bme680_sensor.bme680Status != BME680_OK) {
    if (bme680_sensor.bme680Status < BME680_OK) {
      out_str = "BME680 error code : " + String(bme680_sensor.bme680Status);
      Serial.println(out_str);

    } else {
      out_str = "BME680 warning code : " + String(bme680_sensor.bme680Status);
      Serial.println(out_str);
    }
  }
}

void read_BME680(SensorStateReading* out_state){
    /*
    Read data from the BME680, populate the relevant fields in out_state
    out_readings: pressure, gas_resistance, temp, humidity
    */
    if (bme680_sensor.run()) { // If new data is available, record it
      out_state->pressure = bme680_sensor.pressure;
      out_state->temp = bme680_sensor.temperature;
      out_state->humidity = bme680_sensor.humidity;

    } else {
      //Data is not available. Write the last known values instead.
      check_sensor_status();

      //Make sure that nothing went wrong.
      SensorStateReading* last_state = getLastReading();
      out_state->pressure = last_state->pressure;
      out_state->temp = last_state->temp;
      out_state->humidity = last_state->humidity;
    }
  }

void read_CCS811(SensorStateReading* out_state){
  /*
  Read data from the CCS811, populate the relevant fields in out_state
  */

  //Check to see if data is ready with .dataAvailable()
  if (ccs811_sensor.dataAvailable())
  {
    //If so, have the sensor read and calculate the results.
    //Get them later
    ccs811_sensor.readAlgorithmResults();

    //Returns calculated CO2 reading
    out_state->co2 = ccs811_sensor.getCO2();

    //Returns calculated TVOC reading
    out_state->voc = ccs811_sensor.getTVOC();

  }
}

void setup() {
  Serial.begin(115200);
  Wire.begin(); //Inialize I2C Hardware
  delay(10);

  /*
  Configure I/O pins  
  */
  pinMode(LEDC_OUTPUT_IO, OUTPUT);
  pinMode(INDICATOR_LED_PIN, OUTPUT);
  pinMode(LIGHTPIN, ANALOG);

  /*
  Initialize the CCS811
  */
  if (ccs811_sensor.begin() == false)
  {
    Serial.print("CCS811 error. Please check wiring. Freezing...");
    while (1);
  }

  /*
  Initialize RTC
  */
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }
  rtc.start();

  /*
  Configure BME680 air quality sensor
  */
  bme680_sensor.begin(BME680_I2C_ADDR_SECONDARY, Wire);

  //Tell the sensor which values to record
  bsec_virtual_sensor_t sensorList[10] = {
    BSEC_OUTPUT_RAW_TEMPERATURE,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_RAW_HUMIDITY,
    BSEC_OUTPUT_RAW_GAS,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
    BSEC_OUTPUT_CO2_EQUIVALENT,
    BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  bme680_sensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);

  /*Setup the built-in LED controller to drive the buzzer */
  // Prepare and then apply the LEDC PWM timer configuration
  ledc_timer_config_t ledc_timer = {
    /*speed_mode:*/      LEDC_MODE, 
    /*duty_resolution:*/ LEDC_DUTY_RES,
    /*timer_num:*/       LEDC_TIMER, 
    /*freq_hz:*/         LEDC_FREQUENCY
  };
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  // Prepare and then apply the LEDC PWM channel configuration
  ledc_channel_config_t ledc_channel = {
    LEDC_OUTPUT_IO,
    LEDC_MODE,
    LEDC_CHANNEL_0,
    LEDC_INTR_DISABLE,
    LEDC_TIMER,
    0, // Set duty to 0%
    0
  };
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));

  /*
  Initialize the buzzer duty cycle to 0% (off)
  */
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, 0);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);

  /*
  Configure ESP32 as WiFi access point
  */
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  
  /*
  Attach API callbacks
  */
  server.on("/", handle_OnConnect);
  server.on("/getstate", handle_getstate);
  server.on("/pressure_hist", handle_pressurehist);
  server.on("/humidity_hist", handle_humidityhist);
  server.on("/temp_hist", handle_temphist);
  server.on("/co2_hist", handle_cotwohist);
  server.on("/voc_hist", handle_vochist);
  server.on("/light_hist", handle_lighthist);
  server.onNotFound(handle_NotFound);
  
  server.begin();
  Serial.println("HTTP server started");
}


void loop() {
  server.handleClient(); //Process HTTP requests
  
  /*
  Periodically record sensor readings
  */
  if(millis() - last_read > read_interval){
    if(num_readings < MAX_HIST_SZ){
      Serial.println("Logging!");

      SensorStateReading* next_reading = &history[num_readings]; //Get next sensor data object to write to
      num_readings++;
      
      getCurrentState(next_reading); //Populate readings

      last_read = millis();

      /*
      Check for dangerous conditions.
      */
      if(next_reading->voc > 800){
        //Dangerously high VOC. Start the buzzer and enable LED.
        ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, LEDC_DUTY);
        digitalWrite(INDICATOR_LED_PIN, HIGH);        
      }else{
        //All clear.
        ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, 0);
        digitalWrite(INDICATOR_LED_PIN, LOW);
      }
      ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);  
    }    
  }
}

void handle_OnConnect() {
  server.send(200, "text/html", "Hello world!"); 
}

void handle_getstate(){
  /*
  Return all current sensor values as a json
  */
  result_string = "";

  SensorStateReading curr_state;
  getCurrentState(&curr_state);

  result_string = curr_state.serialize_json();
  
  Serial.println("Returning state data!");
  server.send(200, "application/json", result_string); 
}

void handle_lighthist(){
  /*
  Send previous light sensor data readings as json
  */
  doc.clear();
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].light);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning light history!");
  server.send(200, "application/json", result_string);   
}

void handle_cotwohist(){
  doc.clear();
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].co2);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning CO2 history!");
  server.send(200, "application/json", result_string);     
}

void handle_vochist(){ 
  doc.clear();
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].voc);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning voc history!");
  server.send(200, "application/json", result_string);     
}

void handle_humidityhist(){
  doc.clear();
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].humidity);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning humidity history!");
  server.send(200, "application/json", result_string);      
}

void handle_pressurehist(){ 
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].pressure);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning pressure history!");
  server.send(200, "application/json", result_string);   
}

void handle_temphist(){
doc.clear();
  result_string = "";

  JsonArray data = doc.createNestedArray("data");
  JsonArray timestamps = doc.createNestedArray("timestamps");

  for(int i = 0; i < num_readings; i++){
    data.add(history[i].temp);
    timestamps.add(history[i].timestamp);    
  }

  serializeJson(doc, result_string);
  Serial.println("Returning temp history!");
  server.send(200, "application/json", result_string);   
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}