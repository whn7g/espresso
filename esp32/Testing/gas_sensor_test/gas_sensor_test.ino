#include <Wire.h>
#include <bsec.h>

#define GAS_SDA 21
#define GAS_SCL 22

Bsec iaqSensor;
String gas_output = "";
TwoWire I2CBME = TwoWire(0);

void checkIaqSensorStatus(){
  String gas_output;
  if (iaqSensor.status != BSEC_OK) {
    if (iaqSensor.status < BSEC_OK) {
      gas_output = "BSEC error code : " + String(iaqSensor.status);
      Serial.println(gas_output);

    } else {
      gas_output = "BSEC warning code : " + String(iaqSensor.status);
      Serial.println(gas_output);
    }
  }

  if (iaqSensor.bme680Status != BME680_OK) {
    if (iaqSensor.bme680Status < BME680_OK) {
      gas_output = "BME680 error code : " + String(iaqSensor.bme680Status);
      Serial.println(gas_output);

    } else {
      gas_output = "BME680 warning code : " + String(iaqSensor.bme680Status);
      Serial.println(gas_output);
    }
  }
}

void get_gas_readings(){
    unsigned long time_trigger = millis();
    if (iaqSensor.run()) { // If new data is available, record it
      gas_output = String(time_trigger);
      gas_output += ", " + String(iaqSensor.rawTemperature);
      gas_output += ", " + String(iaqSensor.pressure);
      gas_output += ", " + String(iaqSensor.rawHumidity);
      gas_output += ", " + String(iaqSensor.gasResistance);
      gas_output += ", " + String(iaqSensor.iaq);
      gas_output += ", " + String(iaqSensor.iaqAccuracy);
      gas_output += ", " + String(iaqSensor.temperature);
      gas_output += ", " + String(iaqSensor.humidity);
      gas_output += ", " + String(iaqSensor.staticIaq);
      gas_output += ", " + String(iaqSensor.co2Equivalent);
      gas_output += ", " + String(iaqSensor.breathVocEquivalent);

      Serial.println(gas_output);
      
    } else {
      //Make sure that nothing went wrong.
      checkIaqSensorStatus();
    }
  }


void setup() {
  Serial.begin(115200);
  Serial.println("Addr: ");
  Serial.println(BME680_I2C_ADDR_SECONDARY);
  I2CBME.begin(GAS_SDA, GAS_SCL, 100000);
  iaqSensor.begin(BME680_I2C_ADDR_SECONDARY, I2CBME);
  gas_output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);
  Serial.println(gas_output);
  
    //Tell the sensor which values to record
  bsec_virtual_sensor_t sensorList[10] = {
    BSEC_OUTPUT_RAW_TEMPERATURE,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_RAW_HUMIDITY,
    BSEC_OUTPUT_RAW_GAS,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
    BSEC_OUTPUT_CO2_EQUIVALENT,
    BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
  checkIaqSensorStatus();

  // Print the header
  gas_output = "Timestamp [ms], raw temperature [°C], pressure [hPa], raw relative humidity [%], gas [Ohm], IAQ, IAQ accuracy, temperature [°C], relative humidity [%], Static IAQ, CO2 equivalent, breath VOC equivalent";
  Serial.println(gas_output);

}

void loop() {
  // put your main code here, to run repeatedly:
  delay(5000);
  get_gas_readings();

}
